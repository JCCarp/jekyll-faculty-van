---
layout: default
---

## Quickstart Guide

Click [here](https://bitbucket.org/UBCCS/jekyll-faculty/src/master/) to return to the 
repository website that shows instructions on how to use
and customize this UBC CS faculty and course static website generator yourself.  Browse
the markdown files in particular (```.md``` file extension; start with ```index.md``` -- make sure you view the raw source).

Browse the remainder of this demonstration website to gain a sense of what can
be easily and efficiently generated and maintained (like $x^2 + 1 = 17$, ```while(true) ++i```, and the references below).

---------------
## About Me

<img class="profile-picture" src="headshot.jpg">

I am a professor of computer science at the University of British Columbia.


## Research Interests

While I have a general interest in all areas of computer graphics, the bulk of my research has focussed on computer animation. I am interested in being able to efficiently model and control the motions of humans and animals, both realistic and imaginary. Much of my research makes use of physics-based simulation, given that all motions are shaped to a large extent by physics. However, motions such as walking and leaping are the product of both physics and control. How does one compute the appropriate muscle or motor control in order to yield a desired motion? Much of the work in this field is interdisciplinary, with strong ties to biomechanics, control, machine learning, and robotics.
Recent projects include:

Development of a digital stuntperson.
Falling motions are a logical application of physics-based character animation, given that physics plays a dominant role in such motions and the imminent risk involved in using alternative techniques such as the motion capture of a person falling down a set of stairs. We have developed a physics-based digital stuntperson that can appropriately break his fall when given a hard push in any direction, can pick himself back up off the ground after the fall, and can perform a number of other coordinated tasks.

Virtual puppetry for physics-based characters.
Can one 'steer' a simulated character from the inside using the muscles, much like one can steer an airplane in a flight simulator? Appropriate interfaces allowing for this kind of control would allow users to experiment directly with the dynamics of particular motions. Potential applications include prototyping new motions in sports and developing control strategies for non-anthropomorphic robots or characters.

Multi-modal path planning.
Control and planning are strongly interdependent problems. Consider the example of a character needs to plan her way through a field of large boulders. An efficient path planner needs to have a model of the jumping and climbing abilities of the character. At the same time, it is difficult to define such a model in a compact fashion. We have developed a path planner that can exploit walking, climbing, swinging, and crawling behaviors in order to traverse challening terrain.

Compression techniques for precomputed visibility.
In rendering large models, it is important to identify the small subset of primitives that is visible from a given viewpoint. One approach is to partition the viewpoint space into viewpoint cells and then precompute a visibility table which explicitly records for each viewpoint cell whether or not each primitive is potentially visible. Many computer games use some version of this representation. This project examined ways of compressing such visibility tables in order to produce compact and natural descriptions of potentially-visible sets.

From footprints to animation.
To what extent can one make reasonable inferences about a motion from the footprints alone? We have developed an animation techniques which uses footprint positions and timing as a uniform framework for specifying character motion involving walking, running, and jumping of bipeds and quadrupeds on variable terrain.

## Selected Publications

{% bibliography --file selected.bib %}
